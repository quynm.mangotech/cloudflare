// const express = require('express');
import express from "express";
import configViewEngine from "./configs/viewEngine";
import initAPIRoute from "./route/apiRoute";
const cors = require('cors');
// import cors from 'cors';
const app = express()
const port = 8080;
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


app.use(cors({
    origin: '*'
}));
configViewEngine(app);
initAPIRoute(app);


app.use("/public/images", express.static("./src/public/images"));

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})