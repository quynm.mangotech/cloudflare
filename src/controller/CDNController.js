require('dotenv').config();
const fs = require('fs');
const fetch = require('cross-fetch');
const FormData = require('form-data');
const pinataSDK = require('@pinata/sdk');
const pinata = pinataSDK(process.env.PINATA_API_KEY, process.env.PINATA_API_SECRET);

let increase = 1;
async function uploadCDN(number) {
        const body = new FormData();
        const data = fs.createReadStream(`./src/public/images/${number}.jpg`);
        body.append("file", data);
        body.append("id", `${number}`);
        const res = await fetch(
            `https://api.cloudflare.com/client/v4/accounts/${process.env.CF_IMAGES_ACCOUNT_ID}/images/v1`,
            {
                method: "POST",
                headers: { "Authorization": `Bearer ${process.env.CF_IMAGES_API_KEY}` },
                body,
            }
        );
        if (res.status !== 200 && res.status !== 409) {
            throw new Error("HTTP " + res.status + " : " + await res.text());
        } 
        if (res.status === 409) {
            console.log("Image already exists");
            return;
        }
        const urlImg = `https://imagedelivery.net/${process.env.CF_ACCOUNT_HASH}/${number}/public`
        return urlImg;
}

async function pinIPFS(number, url) {
    let object = fs.readFileSync(`./src/public/json/${number}.json`, 'utf8');
    object = JSON.parse(object)
    object.image = url;
    const dataPinata = await pinPinata(object)
    return dataPinata
}
 
function pinPinata(object) {
    const options = {
        pinataMetadata: {
            name: "My NFT colection",
            keyvalues: {
                customKey: 'customValue',
                customKey2: 'customValue2'
            }
        },
        pinataOptions: {
            cidVersion: 0
        }
    };
    return pinata.pinJSONToIPFS(object, options).then((data) => {
        return data;
    }).catch((err) => {
        console.log(err);
    });
}
async function getResult(req, res) {
    const url = await uploadCDN(increase);
    console.log(url)
    const result = await pinIPFS(increase, url);
    increase++;
    res.send(result)
    return result;
}

module.exports = {
    uploadCDN, getResult
}

