import multer from 'multer';
import fs from "fs"

let uploadPage = async (req, res) => {
    res.render("uploadFile.ejs")
}

let number = 1

const upload = multer().single('profile_pic');

let handleUploadFile = (req, res) => {
    let {name, description, speed, power, eyes} = req.body;
    let jsonObj = {
        name : name,
        description: description,
        image: "",
        attributes: [
            {
                trait_type: "power",
                value: power,
                max_value: "10"
            },
            {
                trait_type: "speed",
                value: speed,
                max_value: "10"
            },
            {
                trait_type: "eyes",
                value: eyes,
            },
        ]
    }
    let jsonData = JSON.stringify(jsonObj)

    fs.writeFile(__dirname + `/../public/json/${number}.json`, jsonData, (err) => {
        if (err) console.log(err);
        number++;
        console.log('File is created successfully.');
    });

    upload(req, res, function(err) {
        if (req.fileValidationError) {
            return res.send(req.fileValidationError);
        }
        else if (!req.file) {
            return res.send('Please select an image to upload');
        }
        else if (err instanceof multer.MulterError) {
            return res.send(err);
        }
        res.send("Successfully");
    });

}

module.exports = {
    uploadPage, handleUploadFile
}