import express from "express";
import APIController from '../controller/APIController';
import CDNController from '../controller/CDNController';
import appRoot from "app-root-path";
import path from "path";
import multer from 'multer';
let router = express.Router();

let number = 1;
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, appRoot + "/src/public/images/");
    },

    filename: function(req, file, cb) {
        cb(null, number + path.extname(file.originalname));
        number++;
    }
});

const imageFilter = function(req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
        req.fileValidationError = 'Only image files are allowed!';
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

const initAPIRoute = (app) => {
    router.get('/upload', APIController.uploadPage);
    router.post('/upload-profile-pic',multer({ storage: storage, fileFilter: imageFilter }).single('profile_pic') , APIController.handleUploadFile);
    router.get('/upload-cdn', CDNController.getResult);
    return app.use('/', router)
}

export default initAPIRoute;
